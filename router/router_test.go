package router

import (
	"net/http"
	"reflect"
	"testing"

	"github.com/go-chi/chi/v5"
	"gitlab.com/golight/example-service/modules/quotes/handlers"
)

func TestNewRouter(t *testing.T) {
	type routeInfo struct {
		middlewaresCount int
		method           string
		route            string
	}

	tests := []struct {
		name            string
		wantMiddlewares map[string]routeInfo
	}{
		{
			name: "NewRouter",
			wantMiddlewares: map[string]routeInfo{
				`/random`: {
					middlewaresCount: 1,
					method:           "GET",
					route:            `/random`,
				},
				`/check`: {
					middlewaresCount: 1,
					method:           "GET",
					route:            `/check`,
				},
				`/static/*`: {
					middlewaresCount: 1,
					method:           "GET",
					route:            `/static/*`,
				},
				`/swagger`: {
					middlewaresCount: 1,
					method:           "GET",
					route:            `/swagger`,
				},

			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewRouter(&handlers.QuotesHandler{})
			
			wantCountRoutes := 0
			err := chi.Walk(got, func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
				wantCountRoutes++
				got := routeInfo{middlewaresCount: len(middlewares), method: method}
				want := tt.wantMiddlewares[route]
				if reflect.DeepEqual(got, want) {
					t.Errorf("NewRouter() route got %v, want %v", got, want)
				}
				return nil
			})
			if err != nil {
				t.Error(err)
			}

			if len(tt.wantMiddlewares) != wantCountRoutes {
				t.Errorf("NewRouter() route count is not enough: got %d, want %d", len(tt.wantMiddlewares), wantCountRoutes)
			}
		})
	}
}
