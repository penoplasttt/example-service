package repository

import (
	"context"

	"gitlab.com/golight/example-service/modules/quotes/entity"
)

// Quoteser is auto generated interface for Quotes storage
//
//go:generate mockgen -source=./quotes_interface.go -destination=./mocks/quoter_mock.go -package=mock
type Quoteser interface {
	Ping(ctx context.Context) error
	Count(ctx context.Context) (uint64, error)
	Create(ctx context.Context, dto entity.Quote) (int64, error)
	GetByID(ctx context.Context, quotesID int) (entity.Quote, error)
	GetList(ctx context.Context) ([]*entity.Quote, error)
	Upsert(ctx context.Context, entities []*entity.Quote, opts ...interface{}) error
	InitData(ctx context.Context, entities []*entity.Quote) (bool, error)
}
