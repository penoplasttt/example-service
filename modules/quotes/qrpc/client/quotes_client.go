package client

import (
	"context"
	"log"

	"gitlab.com/golight/example-service/modules/quotes/entity"
	"gitlab.com/golight/example-service/modules/quotes/qrpc/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/types/known/emptypb"
)

// RPCQuoteser интерфейс rpc для клиента цитат
//
//go:generate mockgen -source=./dao.go -destination=./mock/dao_mock.go -package=mock
type RPCQuoteser interface {
	GetRandomQuote(ctx context.Context) (entity.QuoteDTO, error)
	HealthCheck(ctx context.Context) error
}

// QuotesClient имплементация rpc клиента цитат
type QuotesClient struct {
	rpc  pb.QuotesActionsRPCClient
	addr string
}

// NewQuotesClient конструктор rpc клиента цитат
func NewQuotesClient(addr string) *QuotesClient {
	conn, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	conn.Connect()
	c := pb.NewQuotesActionsRPCClient(conn)

	return &QuotesClient{
		rpc:  c,
		addr: addr,
	}
}

// GetRandomQuote rpc запрос на получение случайной цитаты
func (q *QuotesClient) GetRandomQuote(ctx context.Context) (entity.QuoteDTO, error) {
	var quote entity.QuoteDTO
	quoteRPC, err := q.rpc.GetRandom(ctx, &emptypb.Empty{})
	if err != nil {
		return quote, err
	}

	quote.ID = int(quoteRPC.Id)
	quote.Text = quoteRPC.Text
	quote.Author = quoteRPC.Author

	return quote, nil
}

// HealthCheck rpc запрос на проверку доступности клиента
func (q *QuotesClient) HealthCheck(ctx context.Context) error {
	_, err := q.rpc.HealthCheck(ctx, &emptypb.Empty{})
	return err
}
