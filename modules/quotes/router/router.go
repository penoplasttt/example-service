package router

import (
	"net/http"

	"gitlab.com/golight/example-service/modules/quotes/handlers"
	"github.com/go-chi/chi/v5"
)

func QuotesRouter(qh *handlers.QuotesHandler) http.Handler {
	r := chi.NewRouter()
	r.Get("/random", qh.GetRandomQuoteHandler)
	r.Get("/check", qh.CheckHandler)
	return r
}
