package load_balancer

import (
	"context"

	"gitlab.com/golight/example-service/modules/quotes/entity"
	"go.uber.org/zap"
)

// QuotesLB имплементация клиента цитат с балансировкой
type QuotesLB struct {
	lb  LoadBalancer
	log *zap.Logger
}

// NewQuotesClientLB конструктор клиента цитат с балансировкой
func NewQuotesClientLB(log *zap.Logger, balancer LoadBalancer) *QuotesLB {
	return &QuotesLB{
		lb:  balancer,
		log: log,
	}
}

// GetRandomQuote rpc запрос на получение случайной цитаты
func (q *QuotesLB) GetRandomQuote(ctx context.Context) (entity.QuoteDTO, error) {
	client := q.lb.Next()
	if client == nil {
		q.log.Error("no available clients")
		return entity.QuoteDTO{}, &NoAvailableClientsError{}
	}

	return client.GetRandomQuote(ctx)
}

// HealthCheck rpc запрос на проверку доступности клиента
func (q *QuotesLB) HealthCheck(ctx context.Context) error {
	client := q.lb.Next()
	if client == nil {
		q.log.Error("no available clients")
		return &NoAvailableClientsError{}
	}

	return client.HealthCheck(ctx)
}

// NoAvailableClientsError ошибка отсутствия доступных клиентов
type NoAvailableClientsError struct {
}

// Error возвращает текст ошибки
func (e *NoAvailableClientsError) Error() string {
	return "no available clients"
}
